import * as actionTypes from '../actions/actionTypes';

const initialState = {
    token: null,
    refreshToken: null,
    userId: null
}

function authData(state = initialState, action) {
    switch (action.type) {
        case actionTypes.AUTHENTICATE:
            const { localId: userId, idToken: token, refreshToken } = action.authData;
            const newState = { token, refreshToken, userId }
            return newState;
        case actionTypes.LOGOUT:
            return { ...initialState };
        default:
            return state;
    }
}

export default authData;