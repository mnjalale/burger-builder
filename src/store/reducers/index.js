import { combineReducers } from 'redux';

import authData from './authData';
import burger from './burger';
import orders from './orders';

const rootReducer = combineReducers({
    authData,
    burger,
    orders
});

export default rootReducer;