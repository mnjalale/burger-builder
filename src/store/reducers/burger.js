import { cloneDeep } from 'lodash'

import * as actionTypes from '../actions/actionTypes';

const INGREDIENT_PRICES = {
    salad: 0.5,
    bacon: 0.4,
    cheese: 1.3,
    meat: 0.7
};

const initialState = {
    ingredients: null,
    totalPrice: 4
}

function burger(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SET_INGREDIENTS:
            {
                const newState = cloneDeep(state);
                newState.ingredients = action.ingredients;
                newState.totalPrice = 4;
                return newState;
            }
        case actionTypes.ADD_INGREDIENT:
            {
                const newState = cloneDeep(state);
                newState.ingredients[action.ingredient] += 1;
                newState.totalPrice += INGREDIENT_PRICES[action.ingredient];
                return newState;
            }
        case actionTypes.REMOVE_INGREDIENT:
            {
                const newState = cloneDeep(state);
                if (newState.ingredients[action.ingredient] > 0) {
                    newState.ingredients[action.ingredient] -= 1;
                    newState.totalPrice -= INGREDIENT_PRICES[action.ingredient];
                }
                return newState;
            }
        default:
            return state;
    }
}

export default burger;