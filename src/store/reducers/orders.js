import * as actionTypes from '../actions/actionTypes';

function orders(state = [], action) {
    switch (action.type) {
        case actionTypes.GET_ORDERS:
            const orders = Object
                .keys(action.orders)
                .map(key => {
                    return {
                        ...action.orders[key],
                        id: key
                    };
                });

            return orders;
        case actionTypes.ADD_ORDER:
            const newState = [...state, action.order];
            return newState;
        default:
            return state;;
    }
}

export default orders;