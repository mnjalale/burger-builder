import * as actionTypes from './actionTypes'
import axios from '../../axios/axios-orders';

function setIngredients(ingredients) {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients
    }
}

export function initializeIngredients() {
    return async dispatch => {
        try {
            const response = await axios.get('/ingredients.json');
            const ingredients = response.data;
            dispatch(setIngredients(ingredients));
        }
        catch (error) {
            throw error;
        }
    }
}

export function addIngredient(ingredient) {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingredient
    }
}

export function removeIngredient(ingredient) {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingredient
    }
}