// Ingredients
export const SET_INGREDIENTS = 'SET_INGREDIENTS';
export const ADD_INGREDIENT = 'ADD_INGREDIENT';
export const REMOVE_INGREDIENT = 'REMOVE_INGREDIENT';

// Orders
export const GET_ORDERS = 'GET_ORDERS';
export const ADD_ORDER = 'ADD_ORDER';

// Authentication
export const AUTHENTICATE = 'AUTHENTICATE';
export const LOGOUT = 'LOGOUT';