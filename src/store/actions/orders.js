import * as actionTypes from './actionTypes';
import axios from '../../axios/axios-orders';

export function getOrders(token, userId) {
    return async dispatch => {
        try {
            const url = `${getOrdersEndpoint(token)}&orderBy="userId"&equalTo="${userId}"`
            const response = await axios.get(url);
            const orders = response.data;
            dispatch(getOrdersAction(orders));
        }
        catch (error) {
            throw error;
        }
    }
}

export function createOrder(order, token) {
    return async dispatch => {
        try {
            const response = await axios.post(getOrdersEndpoint(token), order);
            const orderId = response.data.name;
            order = { ...order, id: orderId };
            dispatch(addOrder(order));
        }
        catch (error) {
            throw error;
        }
    }
}


// Helper functions
function addOrder(order) {
    return {
        type: actionTypes.ADD_ORDER,
        order
    }
}

function getOrdersAction(orders) {
    return {
        type: actionTypes.GET_ORDERS,
        orders
    }
}

function getOrdersEndpoint(token) {
    return `/orders.json?auth=${token}`;
}