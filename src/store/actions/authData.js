import * as actionTypes from './actionTypes';
import axios from 'axios';

function authenticate(authData) {
    return {
        type: actionTypes.AUTHENTICATE,
        authData
    }
}

function checkAuthTimeout(expirationTime) {
    return dispatch => {
        return setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    }
}

export function logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.LOGOUT
    }
}

export function auth(email, password, isSignUp) {
    return async dispatch => {
        try {
            const body = {
                email,
                password,
                returnSecureToken: true
            }
            const url = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/${isSignUp ? 'signupNewUser' : 'verifyPassword'}?key=AIzaSyAQHpf2PseYx27wfCZbonG9U5W1N4ii4eM`;
            const response = await axios.post(url, body);
            const authData = response.data;
            const expirationDate = new Date(new Date().getTime() + (authData.expiresIn * 1000))
            localStorage.setItem('token', authData.idToken);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', authData.localId);
            dispatch(authenticate(authData));
            dispatch(checkAuthTimeout(authData.expiresIn));
        }
        catch (error) {
            const errorMessage = error.response.data.error.message;
            throw new Error(errorMessage);
        }
    }
}

export function authCheckState() {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout());
        }
        else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate > new Date()) {
                const userId = localStorage.getItem('userId');
                const authData = {
                    localId: userId,
                    idToken: token,
                }

                const secondsToExpiry = (expirationDate.getTime() - (new Date()).getTime()) / 1000;
                dispatch(authenticate(authData));
                dispatch(checkAuthTimeout(secondsToExpiry));
            } else {
                dispatch(logout());
            }
        }

    }
}

