import axios from 'axios';
import { cloneDeep } from 'lodash';
import { apiKey } from '../secret/secret';

const instance = axios.create({
    baseURL: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty'
});

instance.interceptors.request.use(request => {
    const myRequest = cloneDeep(request);
    myRequest.params.apiKey = apiKey;
    return myRequest;
})

export default instance;