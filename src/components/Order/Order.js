import React from 'react';

import styles from './Order.module.css';

const Order = (props) => {
    const { order } = props;
    const ingredients = Object
        .keys(order.ingredients)
        .map(key => {
            return (
                <span
                    style={{
                        textTransform: 'capitalize',
                        display: 'inline-block',
                        margin: '0 8px',
                        border: '1px solid #ccc',
                        padding: '5px'
                    }}
                    key={key} >
                    {key} ({order.ingredients[key]})
                </span>
            );
        });

    return (
        <div className={styles.order}>
            <p>Ingredients: {ingredients}</p>
            <p>Price: <strong>USD {order.price.toFixed(2)}</strong></p>
        </div>
    );
}

export default Order;