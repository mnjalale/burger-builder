import React from 'react';

import styles from './CheckoutSummary.module.css';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';


const CheckoutSummary = (props) => {
    const {
        checkoutCancelled,
        checkoutContinued } = props;

    return (
        <div className={styles.checkoutSummary}>
            <h1>We hope it tastes well!</h1>
            <div style={{ width: '100%', margin: 'auto' }}>
                <Burger />
            </div>
            <Button
                buttonType="danger"
                clicked={checkoutCancelled}>
                CANCEL
            </Button>

            <Button
                buttonType="success"
                clicked={checkoutContinued}>
                CONTINUE
                 </Button>
        </div>
    );
}

export default CheckoutSummary;