export const createInputFormElement = (type, placeholder, validation) => {
    return {
        elementType: 'input',
        elementConfig: {
            type,
            placeholder
        },
        value: '',
        validation,
        valid: false,
        touched: false

    }
}

export const createSelectFormElement = (options, validation) => {
    return {
        elementType: 'select',
        elementConfig: {
            options
        },
        value: '',
        validation,
        valid: false,
        touched: false
    }
}

export const createValidationObject = (required, minLength, maxLength, isEmail, isNumeric) => {
    return {
        required,
        minLength,
        maxLength,
        isEmail,
        isNumeric
    }
}

export const checkValidity = (value, rules) => {
    let isValid = true;

    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
        isValid = value.trim().length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
        isValid = value.trim().length <= rules.minLength && isValid;
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
        const pattern = /^\d+$/;
        isValid = pattern.test(value) && isValid
    }

    return isValid;
}