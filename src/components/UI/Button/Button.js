import React from 'react';

import styles from './Button.module.css';

const Button = (props) => {
    const { buttonType, children, clicked, disabled } = props;
    return (
        <button
            disabled={disabled}
            className={[styles.button, styles[buttonType]].join(' ')}
            onClick={clicked}>
            {children}
        </button>
    )
}

export default Button;