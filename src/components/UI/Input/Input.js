import React from 'react';

import styles from './Input.module.css';

const Input = (props) => {
    const { label, elementType, elementConfig, value, onChange, invalid } = props;
    let inputElement = null;
    const inputClasses = [styles.inputElement]

    if (invalid) {
        inputClasses.push(styles.invalid);
    }

    switch (elementType) {
        case 'input':
            inputElement = (
                <input
                    className={inputClasses.join(' ')}
                    {...elementConfig}
                    value={value}
                    onChange={onChange} />
            )
            break;
        case 'textarea':
            inputElement = (
                <textarea
                    className={inputClasses.join(' ')}
                    {...elementConfig}
                    value={value}
                    onChange={onChange} />
            )
            break;
        case 'select':
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    value={value}
                    onChange={onChange}>
                    {
                        elementConfig.options.map(option => {
                            return (
                                <option
                                    key={option.value}
                                    value={option.value}>
                                    {option.displayValue}
                                </option>
                            )
                        })
                    }
                    <option></option>
                </select>
            )
            break;
        default:
            inputElement = (
                <input
                    className={inputClasses.join(' ')}
                    {...elementConfig}
                    value={value}
                    onChange={onChange} />
            )
            break;
    }

    return (
        <div className={styles.input}>
            <label className={styles.label}>{label}</label>
            {inputElement}
        </div>
    );
}

export default Input;