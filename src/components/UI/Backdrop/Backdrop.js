import React from 'react';

import styles from './Backdrop.module.css';

const Backdrop = (props) => {
    const { show, clicked } = props;
    let display = show ?
        <div
            className={styles.backdrop}
            onClick={clicked}>
        </div>
        : null;

    return display;
}

export default Backdrop;