import React, { Fragment } from 'react';
import { connect } from 'react-redux'
import Button from '../../UI/Button/Button';

const OrderSummary = (props) => {
    const { ingredients, purchaseCancelled, purchaseContinued, totalPrice } = props;
    const ingredientsList = Object
        .keys(ingredients)
        .map((key, index) => {
            return (
                <li key={index}>
                    <span style={{ textTransform: 'capitalize' }}>
                        {`${(key)}: ${ingredients[key]}`}
                    </span>
                </li>
            )
        });
    return (
        <Fragment>
            <h3>Your Order</h3>
            <p>A delicious burger with the following ingredients:</p>
            <ul>
                {
                    ingredientsList
                }
            </ul>
            <p><strong>Totlal Price: {totalPrice.toFixed(2)}</strong></p>
            <p>Continue to Checkout?</p>
            <Button buttonType="danger" clicked={purchaseCancelled} >CANCEL</Button>
            <Button buttonType="success" clicked={purchaseContinued}>CONTINUE</Button>
        </Fragment>
    )
}

function mapStateToProps(state) {
    return {
        ingredients: state.burger.ingredients,
        totalPrice: state.burger.totalPrice
    }
}

export default connect(mapStateToProps, null)(OrderSummary);