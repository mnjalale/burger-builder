import React from 'react';
import { connect } from 'react-redux';

import styles from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const Burger = (props) => {

    const { ingredients } = props;

    const transformIngredientsArray = Object
        .keys(ingredients)
        .map(ingredientKey => {
            const ingredientCount = ingredients[ingredientKey];
            const ingredientsArray = [...Array(ingredientCount)].map(() => ingredientKey);
            return ingredientsArray;
        });

    const transformIngredients = transformIngredientsArray.length > 0 ?
        transformIngredientsArray.reduce((prev, next) => [...prev, ...next]) :
        [];

    let ingredientsDisplay = <p>Please start adding ingredients</p>
    if (transformIngredients.length > 0) {
        ingredientsDisplay = (
            transformIngredients.map((ingredient, index) => {
                return <BurgerIngredient key={index} type={ingredient} />
            })
        );
    }

    return (
        <div className={styles.burger}>
            <BurgerIngredient type="bread-top" />
            {
                ingredientsDisplay
            }
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
}

function mapStateToProps(state) {
    return {
        ingredients: state.burger.ingredients
    }
}

export default connect(mapStateToProps, null)(Burger);