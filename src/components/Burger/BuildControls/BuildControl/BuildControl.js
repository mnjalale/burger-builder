import React from 'react';

import styles from './BuildControl.module.css';

const BuildControl = (props) => {
    const { label, addIngredient, removeIngredient, ingredientCount } = props;
    return (
        <div className={styles.buildControl} >
            <div className={styles.label} >{label}</div>
            <button
                className={styles.less}
                onClick={removeIngredient}
                disabled={ingredientCount === 0}>
                Less
            </button>
            <button
                className={styles.more}
                onClick={addIngredient}>
                More
            </button>
        </div>
    )
}

export default BuildControl;