import React from 'react';

import styles from './BuildControls.module.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' },
];

const BuildControls = (props) => {
    const {
        addIngredient,
        removeIngredient,
        ingredients,
        totalPrice,
        isAuthenticated,
        purchasable,
        purchase } = props;
    return (
        <div className={styles.buildControls} >
            <p>Current price: <strong>{totalPrice.toFixed(2)}</strong></p>
            {
                controls.map((control, index) => (
                    <BuildControl
                        key={index}
                        label={control.label}
                        ingredientCount={ingredients[control.type]}
                        addIngredient={() => addIngredient(control.type)}
                        removeIngredient={() => removeIngredient(control.type)} />))
            }
            <button
                className={styles.orderButton}
                disabled={!purchasable}
                onClick={purchase}>
                {isAuthenticated ? 'ORDER NOW' : 'SIGN UP TO ORDER'}
            </button>
        </div>
    )
};


export default BuildControls;

