import React from 'react';

import styles from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';



const Toolbar = (props) => {
    const { drawerToggleClicked, isAuthenticated } = props;
    return (
        <header className={styles.toolbar}>
            <DrawerToggle clicked={drawerToggleClicked} />
            <div className={styles.logo} >
                <Logo />
            </div>
            <nav className={styles.desktopOnly}>
                <NavigationItems isAuthenticated={isAuthenticated} />
            </nav>
        </header>
    )
}

export default Toolbar;