import React from 'react';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Backdrop/Backdrop';
import styles from './SideDrawer.module.css'

const SideDrawer = (props) => {
    let attachedClasses = [styles.sideDrawer, styles.close];
    const { closed, open, isAuthenticated } = props;

    if (open) {
        attachedClasses = [styles.sideDrawer, styles.open];
    }

    return (
        <div>
            <Backdrop show={open} clicked={closed} />
            <div className={attachedClasses.join(' ')}>
                <div className={styles.logo}>
                    <Logo />
                </div>
                <nav>
                    <NavigationItems onClick={closed} isAuthenticated={isAuthenticated} />
                </nav>
            </div>
        </div>
    );
}

export default SideDrawer;