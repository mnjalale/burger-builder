import React from 'react';

import styles from './DrawerToggle.module.css';
const DrawerToggle = (props) => {
    const { clicked } = props;
    return (
        <div className={styles.drawerToggle} onClick={clicked}>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
}

export default DrawerToggle;