import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './NavigationItem.module.css';

const NavigationItem = (props) => {
    const { link, children } = props;
    return (
        <li className={styles.navigationItem} >
            <NavLink exact to={link} activeClassName={styles.active}>
                {children}
            </NavLink>
        </li>
    );
}

export default NavigationItem;