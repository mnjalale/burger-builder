import React from 'react';

import NavigationItem from './NavigationItem/NavigationItem';
import styles from './NavigationItems.module.css';

const NavigationItems = (props) => {
    const { isAuthenticated, onClick } = props;
    return (
        <ul className={styles.navigationItems} onClick={onClick}>
            <NavigationItem link="/">Burger Builder</NavigationItem>
            {isAuthenticated && <NavigationItem link="/orders">Orders</NavigationItem>}
            {!isAuthenticated
                ? <NavigationItem link="/auth">Authenticate</NavigationItem>
                : <NavigationItem link="/logout">Logout</NavigationItem>}
        </ul>
    );
}

export default NavigationItems;