import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { logout } from '../../../store/actions';

class Logout extends Component {

    componentDidMount() {
        this.props.logout();
    }

    render() {
        return <Redirect to="/" />
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ logout }, dispatch)
}

export default connect(null, mapDispatchToProps)(Logout);