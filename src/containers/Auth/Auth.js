import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';

import styles from './Auth.module.css';
import withErrorHandler from '../../hoc/withErrorHandlers/withErrorHandlers';
import { auth } from '../../store/actions/authData';
import axios from '../../axios/axios-orders';
import * as uiHelper from '../../components/UI/UIHelper/UIHelper';
import Spinner from '../../components/UI/Spinner/Spinner';
import Button from '../../components/UI/Button/Button';
import Input from '../../components/UI/Input/Input';

class Auth extends Component {
    state = {
        authForm: {
            email: uiHelper.createInputFormElement('email', 'Email Address', uiHelper.createValidationObject(true, false, false, true)),
            password: uiHelper.createInputFormElement('password', 'Password', uiHelper.createValidationObject(true, 7))
        },
        formIsValid: false,
        loading: false,
        isSignUp: false,
        errorMessage: '',
        returnUrl: '/'
    }

    componentDidMount() {
        const queryParams = new URLSearchParams(this.props.location.search);
        // queryParams.entries()
        const returnUrl = queryParams.get('returnUrl');
        if (returnUrl) {
            this.setState({ returnUrl: returnUrl });
        }
    }

    switchAuthModeHandler = () => {
        this.setState((state) => {
            return {
                isSignUp: !state.isSignUp
            }
        });
    }

    authHandler = (event) => {
        event.preventDefault();

        this.setState({ loading: true });
        this.props.auth(this.state.authForm.email.value, this.state.authForm.password.value, this.state.isSignUp)
            .then(() => {
                this.setState({ loading: false, errorMessage: '' });
                this.props.history.push(this.state.returnUrl);
            })
            .catch((error) => {
                const errorMessage = error.message.split('_').join(' ').toLowerCase();
                this.setState({ loading: false, errorMessage: errorMessage });
            });
    }

    inputChangedHandler = (event, key) => {
        const authForm = cloneDeep(this.state.authForm);
        const value = event.target.value;
        authForm[key].value = value;
        authForm[key].valid = uiHelper.checkValidity(value, authForm[key].validation);
        authForm[key].touched = true;

        let formIsValid = true;

        for (const inputElement in authForm) {
            if (!authForm[inputElement].valid) {
                formIsValid = false;
                break;
            }
        }

        this.setState({ authForm: authForm, formIsValid: formIsValid });
    }

    render() {
        let form = <Spinner />
        const authForm = { ...this.state.authForm };
        let errorMessage = null;
        if (this.state.errorMessage) {
            errorMessage = <p style={{ textTransform: 'capitalize', color: 'red' }}>{this.state.errorMessage}</p>
        }

        if (!this.state.loading) {
            form = (
                <Fragment>
                    <form onSubmit={this.authHandler}>
                        {
                            Object
                                .keys(authForm)
                                .map(key => {
                                    return (
                                        <Input
                                            key={key}
                                            elementType={authForm[key].elementType}
                                            elementConfig={authForm[key].elementConfig}
                                            value={authForm[key].value}
                                            invalid={!authForm[key].valid && authForm[key].touched}
                                            onChange={(event) => {
                                                this.inputChangedHandler(event, key)
                                            }}
                                        />
                                    )
                                })
                        }

                        <Button
                            disabled={!this.state.formIsValid}
                            buttonType="success">
                            SIGN {this.state.isSignUp ? "UP" : "IN"}
                        </Button>
                    </form>
                    <Button
                        buttonType="danger"
                        clicked={this.switchAuthModeHandler}>
                        SWITCH TO SIGN {this.state.isSignUp ? 'IN' : 'UP'}
                    </Button>
                </Fragment>
            )
        }

        return (
            <div className={styles.auth}>
                {errorMessage}
                {/* <h4>Enter your Auth Data</h4> */}
                {form}
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ auth }, dispatch)
}

export default connect(null, mapDispatchToProps)(withErrorHandler(Auth, axios));