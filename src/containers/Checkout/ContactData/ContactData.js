import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';
import Button from '../../../components/UI/Button/Button';
import styles from './ContactData.module.css';
import axios from '../../../axios/axios-orders';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import * as uiHelper from '../../../components/UI/UIHelper/UIHelper';
import { createOrder } from '../../../store/actions';
import withErrorHandler from '../../../hoc/withErrorHandlers/withErrorHandlers';

class ContactData extends Component {
    state = {
        orderForm: {
            name: uiHelper.createInputFormElement('text', 'Your Name', uiHelper.createValidationObject(true)),
            email: uiHelper.createInputFormElement('email', 'Your Email', uiHelper.createValidationObject(true, false, false, true)),
            street: uiHelper.createInputFormElement('text', 'Street', uiHelper.createValidationObject(true)),
            postalCode: uiHelper.createInputFormElement('text', 'Postal Code', uiHelper.createValidationObject(true, 5, 5, false, true)),
            city: uiHelper.createInputFormElement('text', 'City', uiHelper.createValidationObject(true)),
            country: uiHelper.createInputFormElement('text', 'Country', uiHelper.createValidationObject(true)),
            deliveryMethod: uiHelper.createSelectFormElement([
                { value: 'fastest', displayValue: 'Fastest' },
                { value: 'cheapest', displayValue: 'Cheapest' },
            ], uiHelper.createValidationObject(true)),
        },
        formIsValid: false,
        loading: false
    }

    orderHandler = (event) => {
        event.preventDefault();

        const formData = {};

        for (const formElement in this.state.orderForm) {
            formData[formElement] = this.state.orderForm[formElement].value
        }

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
            customer: formData,
            deliveryMethod: this.state.orderForm.deliveryMethod.value,
            userId: this.props.authData.userId
        }

        this.setState({ loading: true });
        this.props.createOrder(order, this.props.authData.token)
            .then(() => {
                this.setState({ loading: false });
                this.props.history.push('/');
            })
            .catch((error) => {
                this.setState({ loading: false });
            });
    }


    inputChangedHandler = (event, key) => {
        const orderForm = cloneDeep(this.state.orderForm);
        const value = event.target.value;
        orderForm[key].value = value;
        orderForm[key].valid = uiHelper.checkValidity(value, orderForm[key].validation);
        orderForm[key].touched = true;

        let formIsValid = true;

        for (const inputElement in orderForm) {
            if (!orderForm[inputElement].valid) {
                formIsValid = false;
                break;
            }
        }

        this.setState({ orderForm: orderForm, formIsValid: formIsValid });
    }

    render() {
        let form = <Spinner />
        const orderForm = { ...this.state.orderForm };

        if (!this.state.loading) {
            form = (
                <form onSubmit={this.orderHandler}>
                    {
                        Object
                            .keys(orderForm)
                            .map(key => {
                                return (
                                    <Input
                                        key={key}
                                        elementType={orderForm[key].elementType}
                                        elementConfig={orderForm[key].elementConfig}
                                        value={orderForm[key].value}
                                        invalid={!orderForm[key].valid && orderForm[key].touched}
                                        onChange={(event) => {
                                            this.inputChangedHandler(event, key)
                                        }}
                                    />
                                )
                            })
                    }

                    <Button
                        disabled={!this.state.formIsValid}
                        buttonType="success">
                        ORDER
                    </Button>
                </form>
            )
        }

        return (
            <div className={styles.contactData}>
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        ingredients: state.burger.ingredients,
        totalPrice: state.burger.totalPrice,
        authData: state.authData
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ createOrder }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(withRouter(ContactData), axios));