import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios/axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandlers/withErrorHandlers';
import { initializeIngredients, addIngredient, removeIngredient } from '../../store/actions';



class BurgerBuilder extends Component {

    state = {
        purchasable: false,
        purchasing: false,
        loading: false,
        error: false
    }

    componentDidMount() {
        this.props.initializeIngredients()
            .then(() => { })
            .catch((error) => {
                this.setState({ error: true })
            });
    }

    addIngredientHandler = (ingredient) => {
        this.props.addIngredient(ingredient);
        this.setState({ purchasable: true });
    }

    removeIngredientHandler = (ingredient) => {
        this.props.removeIngredient(ingredient);

        // Check if there are any ingredients are selected
        const totalIngredientsCount = Object
            .values(this.props.ingredients)
            .reduce((prev, current) => prev + current);

        this.setState({
            purchasable: totalIngredientsCount > 0
        });
    }

    purchaseHandler = () => {
        if (this.props.isAuthenticated) {
            this.setState({ purchasing: true });
        } else {
            this.props.history.push({
                pathname: '/auth',
                search: '?returnUrl=/checkout'
            });
        }
    }

    purchaseCancelledHandler = () => {
        this.setState({ purchasing: false });
    }

    purchaseContinuedHandler = () => {
        this.props.history.push('/checkout');
    }

    render() {
        let orderSummary = null;
        let burger = this.state.error ? <p>Ingredients can't be loaded</p> : <Spinner />;

        if (this.props.ingredients) {
            burger = (
                <div>
                    <Burger />
                    <BuildControls
                        ingredients={this.props.ingredients}
                        totalPrice={this.props.totalPrice}
                        isAuthenticated={this.props.isAuthenticated}
                        addIngredient={(ingredient) => {
                            this.addIngredientHandler(ingredient);
                        }}
                        removeIngredient={(ingredient) => {
                            this.removeIngredientHandler(ingredient);
                        }}
                        purchasable={this.state.purchasable}
                        purchase={this.purchaseHandler} />
                </div>
            );

            orderSummary = (
                <OrderSummary
                    purchaseCancelled={this.purchaseCancelledHandler}
                    purchaseContinued={this.purchaseContinuedHandler}
                />
            );
        }

        if (this.state.loading) {
            orderSummary = <Spinner />
        }

        return (
            <div>
                <Modal
                    show={this.state.purchasing}
                    modalClosed={this.purchaseCancelledHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        ingredients: state.burger.ingredients,
        totalPrice: state.burger.totalPrice,
        isAuthenticated: !!state.authData.token
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ initializeIngredients, addIngredient, removeIngredient }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));