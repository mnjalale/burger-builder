import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Order from '../../components/Order/Order';
import axios from '../../axios/axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandlers/withErrorHandlers';
import { getOrders } from '../../store/actions';

class Orders extends Component {
    state = {
        loading: false
    }

    componentDidMount() {
        this.setState({ loading: true });
        this.props.getOrders(this.props.token, this.props.userId)
            .then(() => {
                this.setState({ loading: false });
            })
            .catch(error => {
                this.setState({ loading: false });
                console.log(error);
            });
    }

    render() {
        let orders = <Spinner />;
        if (!this.state.loading) {
            orders = (
                <Fragment>
                    {
                        this.props.orders.map((order) => {
                            return <Order
                                key={order.id}
                                order={order} />
                        })
                    }
                </Fragment>
            )
        }

        return (
            <div>
                {orders}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        orders: state.orders,
        token: state.authData.token,
        userId: state.authData.userId
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getOrders }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axios));